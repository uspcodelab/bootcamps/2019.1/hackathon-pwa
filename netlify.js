const NetlifyAPI = require('netlify');
const client = new NetlifyAPI('2dbf8c5c05d62288a81d9b20143e04348ae483dee8f55eab0ce694aaee455a6a');

const LOG_HEADER = "> [hacknizer pwa-gen deploy]";

module.exports = {
	listSites: async function() {
		try {
			var sites = await client.listSites(); 
			sites.forEach(function(site) {
        console.log(`${LOG_HEADER}
          \nID: ${site['id']}
			    \nName: ${site['name']}
			    \nURL: ${site['url']}`);
			});
		} catch(error) {
			console.error(`${LOG_HEADER}: ${error}`);
		}
  },
  createSite: async function() {
    try {
      const site = await client.createSite({
        body: {
          name: 'HackathonUSP 1000.15'
        }
      });
      console.log(`${LOG_HEADER}
        ID: ${site['id']}
        Name: ${site['name']}
        URL: ${site['url'].replace(' ', "-")}
        Site ID: ${site['site_id']}`
      );
    } catch(error) {
      console.error(`${LOG_HEADER}: ${error}`);
    }
  },
  createAndDeploy: async function(sitename) {
    console.log(`${LOG_HEADER} Init deploy`)

    sitename = sitename.replace(' ', "-");
    try {
      const site = await client.createSite({
        body: {
          name: sitename
        }
      });

      console.log(`${LOG_HEADER}
        ID: ${site['id']}
        Name: ${site['name']}
        URL: ${site['url'].replace(' ', "-")}
        Site ID: ${site['site_id']}`
      );
      var siteID = site['site_id'];

      const deploy = await client.deploy(siteID, "dist/");

      console.log(`${LOG_HEADER} Site ${deploy['url']} have been deployed`);
    } catch(error) {
      console.error(`${LOG_HEADER}: ${error}`);
      return;
    }

    console.log(`${LOG_HEADER} Deploy success!`);
    console.log(`${LOG_HEADER} End deploy`);
  }
};
