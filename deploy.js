const pwaGen = require('./pwa-gen');
const builder = require('./builder');
const netlify = require('./netlify');
const postNetlify = require('./post-netlify.js');

// DUMMY EXAMPLE
const DUMMY = {
  logo: "logo",
  slug: "hackfools",
  editions: [
    {
      name: "2018",
      palette: {
        primary: "#123456",
        secondary: "#654321"
      },
    },
    {
      name: "2019",
      palette: {
        primary: "#ff690a",
        secondary: "#ff69a0"
      },
    },
  ]
};

async function deploy() {
  console.log("> [hacknizer pwa-gen] Init");

  // GENERATE ROUTES, PAGES
  await pwaGen.generate(DUMMY);

  // RUN VUE CLI SERVICE BUILD COMMAND
  await builder.build();

  // UPLOAD TO NETLIFY
  await netlify.createAndDeploy(DUMMY.slug);
  
  // REMOVING GENERATED FILES
  await postNetlify.removeTempFiles();

  console.log("> [hacknizer pwa-gen] The End");
};

deploy();