/* tslint:disable:typedef */

import Vue, { VueConstructor } from 'vue';
import Router from 'vue-router';
import { createRouterLayout } from 'vue-router-layout';
import { routes } from './routes';

const RouterLayout: VueConstructor<Vue> = createRouterLayout((layout: string) => {
  return import('@/layouts/' + layout + '.vue');
});

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: RouterLayout,
      children: routes,
    },
  ],
});
