import { RouteConfig } from 'vue-router';
export const routes: RouteConfig[] = [
  {
    path: '2018',
    component: (): Promise<typeof import('*.vue')> => import('@/pages/Home.vue'),
  },
  {
    path: '2019',
    component: (): Promise<typeof import('*.vue')> => import('@/pages/ComponentList.vue'),
  },

];