/* tslint:disable:no-any */
import VueApollo from 'vue-apollo';

import { ApolloClient } from 'apollo-client';
import { ApolloLink, GraphQLRequest } from 'apollo-link';

import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';

const cache: InMemoryCache = new InMemoryCache();

const authLink: ApolloLink = setContext((_: GraphQLRequest, { headers }: any) => {
  const token: string | null = localStorage.getItem('token');
  return {
    ...headers,
    authorization: token ? `Bearer ${token}` : '',
  };
});

const link: ApolloLink = ApolloLink.from([authLink]);

const apolloClient: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
});

export default new VueApollo({
  defaultClient: apolloClient,
});
