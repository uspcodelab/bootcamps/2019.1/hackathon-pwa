const fs = require('fs-extra');

// CONSTANT VARIABLES
const LOG_HEADER = "> [hacknizer pwa-gen generate]";

const TEMPLATE_PATH = "src/pages/PageTemplate.vue";

const ROUTES_HEADER = `import { RouteConfig } from 'vue-router';\nexport const routes: RouteConfig[] = [\n`;
const ROUTES_END = `\n];`;
const ROUTES_PATH = "src/router/routes.ts";

const COLORS_HEADER = `module.exports = {\n`;
const COLORS_END = `};`;
const COLORS_PATH = "./customColors.js";

module.exports = {
  generate: async function(data) {
    console.log(`${LOG_HEADER} Init generate`);
    
    fs.writeFileSync(ROUTES_PATH, ROUTES_HEADER);
    fs.writeFileSync(COLORS_PATH, COLORS_HEADER);

    data.editions.forEach((ed) => {
      const filename = `src/pages/_${ed.name}.vue`;
      const primaryClassname = `primary-${ed.name}`;
      const primaryValue = ed.palette.primary;
      const secondaryClassname = `secondary-${ed.name}`;
      const secondaryValue = ed.palette.secondary;

      // Duplicate template .vue page
      fs.copyFileSync(TEMPLATE_PATH, filename);

      // Add primary and secondary colors to custom color file
      fs.appendFileSync(COLORS_PATH, `
        '${primaryClassname}': '${primaryValue}',\n
        '${secondaryClassname}': '${secondaryValue}',\n`);

      // Inject colors into duplicated .vue page
      // fs.readFile(filename, 'utf8', function (err,data) {
      //   if (err) {
      //     return console.log(`${LOG_HEADER}: ${err}`);
      //   }

      //   var result = data.replace(/\*\*\*INSERT CLASS\*\*\*/g, primaryClassname);
      //   result = result.replace(/\*\*\*INSERT COLOR\*\*\*/g, primaryValue);
      
      //   fs.writeFileSync(filename, result, 'utf8', function (err) {
      //     if (err) return console.log(`${LOG_HEADER}: ${err}`);
      // });
      // });
      let content = fs.readFileSync(filename, 'utf8', function (err) {
        if (err) return console.log(`${LOG_HEADER}: ${err}`);
      });
      content = content.replace(/\*\*\*INSERT CLASS\*\*\*/g, primaryClassname);
      content = content.replace(/\*\*\*INSERT COLOR\*\*\*/g, primaryValue);
      fs.writeFileSync(filename, content, 'utf8', function (err) {
        if (err) return console.log(`${LOG_HEADER}: ${err}`);
      });

      // Create new route at routes file
      fs.appendFileSync(ROUTES_PATH,
        `  {
        path: '${ed.name}',
        component: (): Promise<typeof import('*.vue')> => import('@/pages/_${ed.name}.vue'),
      },\n`
      )
    });

    fs.appendFileSync(ROUTES_PATH, ROUTES_END);
    fs.appendFileSync(COLORS_PATH, COLORS_END);

    console.log(`${LOG_HEADER} End generate`);
  }
};