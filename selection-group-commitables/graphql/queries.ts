import gql from 'graphql-tag';

export const GET_PARTICIPANTS_QUERY = gql`
  mutation GetParticipants($editionId: String!) {
    getParticipants(editionId: $editionId) {
      id
    }
  }
`;
