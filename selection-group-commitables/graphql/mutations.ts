import gql from 'graphql-tag';

export const CREATE_PARTICIPANT_MUTATION = gql`
  mutation CreateParticipant(
    $userId: String!
    $editionId: String!
    $role: String!
  ) {
    createParticipant(
      participant: { userId: $userId, editionId: $editionId, role: $role }
    ) {
      userId
      editionId
      role
      id
    }
  }
`;

export const CHANGE_PARTICIPANT_ROLE_MUTATION = gql`
  mutation ChangeParticipantRole($id: String!, $role: String!) {
    changeParticipantRole(id: $id, role: $role) {
      userId
      editionId
      role
      id
    }
  }
`;
