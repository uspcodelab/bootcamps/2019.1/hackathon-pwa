const fs = require('fs-extra');

const PAGE_PATH = 'src/pages';
const LOG_HEADER = '> [hacknizer pwa-gen post-deploy]';

module.exports = {
  removeTempFiles: async function() {
    console.log(`${LOG_HEADER} Init post-deploy`);

    fs.remove('./dist', err => {
      if (err) return console.error(`${LOG_HEADER} Error trying to remove dist/ folder: ${err}`);
      console.log(`${LOG_HEADER} Successfully deleted dist/ folder`);
    });

    fs.readdirSync(PAGE_PATH).forEach(file => {
      if (file.includes('_')) {
        const filepath = `${PAGE_PATH}/${file}`;

        fs.remove(filepath, err => {
          if (err) return console.log(`${LOG_HEADER} Error trying to remove ${filepath} : ${err}`);
          
          console.log(`${LOG_HEADER} Successfully deleted ${filepath}`);
        });
      } 
    });

    console.log(`${LOG_HEADER} End post-deploy`);
  }
};