const { execSync } = require('child_process');

const LOG_HEADER = "> [hacknizer pwa-gen build]";

module.exports = {
  build: async function() {
    console.log(`${LOG_HEADER} Init build`);

    try {
      console.log(`${LOG_HEADER} Running 'npm run build'`);
      execSync('npm run build');
    } catch (ex) {
      console.log(`${LOG_HEADER} Build caught exception : ${ex}`);
    }

    console.log(`${LOG_HEADER} End build`);
  }
}